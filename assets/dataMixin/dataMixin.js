export const dataMixin = {
    data() {
        return {
            listCategory : [{id: 0, categoryName: "Xây dựng"}, 
                            {id: 1, categoryName: "Thời tiết"}, 
                            {id: 2, categoryName:"Kinh tế"}, 
                            {id: 3, categoryName: "Giải trí"},
                            {id: 4, categoryName: "Thể thao"},
                            {id: 5, categoryName: "Xã hội"}, 
                            {id: 6, categoryName: "Giáo dục"}, 
                            {id: 7, categoryName:"Y học"},
                            {id: 8, categoryName: "Drama"}, 
                            {id: 9, categoryName: "Du lịch"}, 
                            {id: 10, categoryName: "Động vật"}, 
                            {id: 11, categoryName: "Công nghệ"}, 
                            {id: 12, categoryName: "Xe"}],

            listPositions: [{id: 1, positionName: "Việt Nam"}, 
                            {id: 2, positionName: "Châu Á"}, 
                            {id: 3, positionName: "Châu Âu"}, 
                            {id: 4, positionName: "Châu Mỹ"}],
            optionPosition: [
                { text: 'Việt Nam', value: '1' },
                { text: 'Châu Âu', value: '2' },
                { text: 'Châu Á', value: '3' },
                { text: 'Châu Mỹ', value: '4' }
                ],
            optionPublic: [
                { text: 'Yes', value: true },
                { text: 'No', value: false},
            ],
            listErrors:[
                { text: 'Hãy điền đầy đủ tiêu đề', id:1},
                { text: 'Hãy điền đầy đủ mô tả ngắn', id:2},
                { text: 'Hãy điền đầy đủ mô tả dài', id:3},
                { text: 'Hãy tích ô vị trí', id:4},
                { text: 'Hãy điền public hay không', id:5},
                { text: 'Hãy điền chủ đề hướng đến', id:6},
                { text: 'Hãy điền ngày phát hành', id:7},
            ]

        }
    },

    computed: {
        
    }
}
export const linkApi = "https://localhost:3000/blogs/";
